# Image Classification with Neural Networks

![ic11](/uploads/7277be3d9f7436c54aa0396743e4f60a/ic11.jpg)

Image Classification Classification System consists of database that contains predefined patterns that compares with detected object to classify in to proper category.

**Different Image Processing Steps**:


* **Digital Data:** 

An image is captured by using digital camera or any mobile phone camera.

* **Pre-processing:** 

Improvement of the image data that surpresses the unwanted distortions and enhances the important image features.

![ic-1](/uploads/c892c414f837a40296a65ed1ac6300cc/ic-1.jpg)

* **Feature extraction:** 

It is the process by which certain features of interest within an image sample are detected and represented for further processing.

![ic-2](/uploads/6c1ccfdcf775b9567370cba691ab99ee/ic-2.png)

* **Selection of training data:** 

Selection of the particular attribute which best describes the pattern. 

![ic-3](/uploads/a6f6b30e1af782c268b34d38cc397c1e/ic-3.jpeg)

* **Decision and Classification:** 

Categorizes detected objects into predefined classes by using suitable method that compares the image patterns with the target patterns. Classification refers to the task of extracting 

![ic-4](/uploads/051034b0b0130ee4a5c22dce9f0ba116/ic-4.png)information classes from a multiband raster image.



*  **Accuracy assessment:** An accuracy assessment is realized to identify possible sources of errors and as an indicator used in comparisons.
