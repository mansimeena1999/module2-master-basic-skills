# FACE RECOGNITION

## WHAT IS FACE RECOGNITION?

Face recognition is a **technology capable of identifying or verifying a subject through an image, video or any audiovisual element of his face**.<br> Generally, this identification is used to access an application, system or service.

It is a method of **biometric identification** that uses that body measures, in this case face and head, to **verify the identity of a person through its facial biometric pattern and data**. 

The technology collects a set of unique biometric data of each person associated with their face and facial expression to identify, verify and/or authenticate a person.

![fr1](/uploads/cf825edfd6e13987bf483b41023c3081/fr1.jpg)

## FACE RECOGNITION SYSTEM
The procedure simply requires any device that has digital photographic technology to generate and obtain the images and data necessary to create and record the biometric facial pattern of the person that needs to be identified.

Unlike other identification solutions such as passwords, verification by email, selfies or images, or fingerprint identification, **Biometric facial recognition uses unique mathematical and dynamic patterns that make this system one of the safest and most effective ones**.

The objective of face recognition is, from the incoming image, to find a series of data of the same face in a set of training images in a database. The great difficulty is ensuring that this process is carried out in real-time, something that is not available to all biometric facial recognition software providers.

The facial recognition process can perform two variants depending on when it is performed:

- The one in which, for the first time, a facial recognition system addresses a face to register it and associate it with an identity, in such a way that it is recorded in the system. This process is also known as **digital onboarding** with facial recognition.

- The variant in which the user is **authenticated**, prior to being registered. In this process, the incoming data from the camera is crossed with the existing data in the database. If the face matches an already registered identity, the user is granted access to the system with his credentials.

![fr2](/uploads/ec914cdb261d50cb7462646f0dd21db6/fr2.jpg)

## HOW DOES FACE RECOGNITION WORK?
Face recognition systems **capture an incoming image from a camera device** in a two-dimensional or three-dimensional way depending on the characteristics of the device. 

These ones compare the relevant information of the incoming image signal in real-time in photo or video in a database, being much more reliable and secure than the information obtained in a static image. This biometric facial recognition procedure requires an internet connection since the database cannot be located on the capture device as it is hosted on servers.

In this comparison of faces, it **analyses mathematically** the incoming image without any margin of error and it **verifies that the biometric data** matches the person who must use the service or is requesting access to an application, system or even building. 

Thanks to the use of **artificial intelligence (AI) and machine learning technologies**, face recognition systems can operate with the highest safety and reliability standards. Similarly, thanks to the integration of these algorithms and computing techniques, the process can be carried out in real-time.

Face recognition uses focus on verification or authentication. This technology is used, for example, in situations such as:

- **Second authentication factor**, to add extra security in any log-in process.
- **Access to previously contracted online services** (login on online platforms, for example).
- **Access to buildings** (offices, events, facilities of any kind …) and tourist services** (airports, hotels…).

![fr3](/uploads/6a8e114ed6ff16135fc1b209dfc8b5b3/fr3.jpg)

- **Payment method**, both in physical and online stores.

![fr4](/uploads/accb513683c27f0d9d522c746dab6b20/fr4.jpg)

- **Access to mobile applications without a password and locked devices**.

![fr5](/uploads/e01497013e4a40354c775f2364ac3629/fr5.jpg)








